<?php

namespace ArticleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;



class ArticleController extends Controller
{
    
    /**
     *  ShowList
     *  Permet l'affichage de la liste des article
     * 
     */
    public function showListAction()
    {
        $articlesListe = $this->getDoctrine()
                     ->getManager()
                     ->getRepository('ArticleBundle:Article')
                     ->findAll();
        
        
        
        return $this->render('ArticleBundle::index.html.twig', array(
            'articlesListe'=> $articlesListe,
            ));
    }
    /**
     * ShowArticle
     * Permet l'affichage d'un article en fonction de son id
     * Recupere son titre afin de l'afficher 
     * @param type $id
     * @return type
     */
    public function showArticleAction($id){
        $article = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('ArticleBundle:Article')
                        ->find($id);
        
        return $this->render('ArticleBundle::articleShow.html.twig', array(
            'article' =>$article,
            'titlePage' => $article->getTitre(),
            ));
        
                
    }
    
}


